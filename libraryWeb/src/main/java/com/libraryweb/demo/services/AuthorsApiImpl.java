package com.libraryweb.demo.services;

import com.libraryweb.demo.model.AuthorsEntity;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class AuthorsApiImpl implements AuthorsApi {

    private AuthorsRepository authorsRepository;

    @Autowired
    public AuthorsApiImpl(AuthorsRepository authorsRepository) {
        this.authorsRepository = authorsRepository;
    }

    @Override
    @GetMapping(path = "/allAuthors")
    public ResponseEntity<AuthorDto[]> allAuthors() {
        List<AuthorsEntity> all = authorsRepository.findAll();
        List<AuthorDto> authorList = new ArrayList<>();

        for (AuthorsEntity authorsEntity : all) {
            AuthorDto authorDto = new AuthorDto();
            authorDto.setAuthorId(authorsEntity.getAuthorId());
            BeanUtils.copyProperties(authorsEntity, authorDto);
            authorList.add(authorDto);
        }
        AuthorDto[] array = authorList.stream().toArray(n -> new AuthorDto[n]);


        return new ResponseEntity<AuthorDto[]>(array, HttpStatus.OK);
    }

    @Override
    @PostMapping(path = "/createAuthor")
    public void createBook(@RequestBody AuthorDto booksDto) {
        ModelMapper modelMapper = new ModelMapper();
        AuthorsEntity fromDtoToEntity = modelMapper.map(booksDto, AuthorsEntity.class);
        authorsRepository.saveAndFlush(fromDtoToEntity);

    }

    @Override
    @PutMapping(path = "/editAuthor")
    public void editBook(@RequestBody AuthorDto authorDto) {
        ModelMapper modelMapper = new ModelMapper();
        AuthorsEntity fromDtoToEntity = modelMapper.map(authorDto, AuthorsEntity.class);
        int id = fromDtoToEntity.getAuthorId();
        Optional<AuthorsEntity> authorForChange = authorsRepository.findById(authorDto.getAuthorId());
        authorForChange.get().setAuthorName(authorDto.getAuthorName());
        authorForChange.get().setFirstThreeBooks(authorDto.getFirstThreeBooks());
        authorForChange.get().setRank(authorDto.getRank());
        authorsRepository.saveAndFlush(authorForChange.get());

    }

    @Override
    @RequestMapping(method = RequestMethod.DELETE, value = {"deleteAuthor/{id}"})
    public void delete(@PathVariable Integer id) {
        AuthorsEntity authorsEntity = authorsRepository.getOne(id);
        authorsRepository.delete(authorsEntity);
    }


}
