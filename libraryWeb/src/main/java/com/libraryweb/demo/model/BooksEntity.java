package com.libraryweb.demo.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table
public class BooksEntity {
    private int bookId;
    private String name;
    private String author;
    private int numberOfPages;
    private String summary;
    private List<AuthorsEntity> authors = new ArrayList<AuthorsEntity>();

    public BooksEntity() {
    }

    public BooksEntity(int id, String name, String author, int numberOfPages, String summary) {
        this.bookId = id;
        this.name = name;
        this.author = author;
        this.numberOfPages = numberOfPages;
        this.summary = summary;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(updatable = false, nullable = false)
    public int getbookdId() {
        return bookId;
    }

    public void setbookdId(int bookdId) {
        this.bookId = bookdId;
    }

    @Column(nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(nullable = false)
    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Column(nullable = false)
    public int getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(int numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    @Column(nullable = false)
    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    @ManyToMany
    @JoinTable(name = "book_author",
            joinColumns = {@JoinColumn(name = "book_Id")},
            inverseJoinColumns = {@JoinColumn(name = "author_Id")})
    public List<AuthorsEntity> getAuthors() {
        return authors;
    }

    public void setAuthors(List<AuthorsEntity> authors) {
        this.authors = authors;
    }
}
