package com.libraryweb.demo.services;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

public interface BooksApi {
    ResponseEntity<BooksDto[]> allAuthors();

    @PostMapping(path = "/createBook")
    void createBook(@RequestBody BooksDto booksDto);

    @PutMapping(path = "/editBook")
    void editBook(@RequestBody BooksDto booksDto);

    @RequestMapping(method = RequestMethod.DELETE, value = {"delete/{id}"})
    void delete(@PathVariable Integer id);
}
