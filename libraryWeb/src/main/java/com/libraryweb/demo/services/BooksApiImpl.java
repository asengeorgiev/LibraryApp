package com.libraryweb.demo.services;

import com.libraryweb.demo.model.BooksEntity;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class BooksApiImpl implements BooksApi {

    private BooksRepository booksRepository;

    @Autowired
    public BooksApiImpl(BooksRepository booksRepository) {
        this.booksRepository = booksRepository;
    }

    @Override
    @GetMapping(path = "/allBooks")
    public ResponseEntity<BooksDto[]> allAuthors() {
        List<BooksEntity> all = booksRepository.findAll();
        List<BooksDto> bookList = new ArrayList<>();

        for (BooksEntity booksEntity : all) {
            BooksDto booksDto = new BooksDto();
            booksDto.setBookId(booksEntity.getbookdId());
            BeanUtils.copyProperties(booksEntity, booksDto);
            bookList.add(booksDto);
        }
        BooksDto[] array = bookList.stream().toArray(n -> new BooksDto[n]);


        return new ResponseEntity<BooksDto[]>(array, HttpStatus.OK);
    }

    @Override
    @PostMapping(path = "/createBook")
    public void createBook(@RequestBody BooksDto booksDto) {
        ModelMapper modelMapper = new ModelMapper();
        BooksEntity fromDtoToEntity = modelMapper.map(booksDto, BooksEntity.class);
        booksRepository.saveAndFlush(fromDtoToEntity);

    }

    @Override
    @PutMapping(path = "/editBook")
    public void editBook(@RequestBody BooksDto booksDto) {
        ModelMapper modelMapper = new ModelMapper();
        BooksEntity fromDtoToEntity = modelMapper.map(booksDto, BooksEntity.class);
        int id = fromDtoToEntity.getbookdId();
        Optional<BooksEntity> bookForChange = booksRepository.findById(booksDto.getBookId());
        bookForChange.get().setAuthor(booksDto.getAuthor());
        bookForChange.get().setName(booksDto.getName());
        bookForChange.get().setNumberOfPages(booksDto.getNumberOfPages());
        bookForChange.get().setSummary(booksDto.getSummary());
        booksRepository.saveAndFlush(bookForChange.get());

    }

    @Override
    @RequestMapping(method = RequestMethod.DELETE, value = {"deleteBook/{id}"})
    public void delete(@PathVariable Integer id) {
        BooksEntity authorsEntity = booksRepository.getOne(id);
        booksRepository.delete(authorsEntity);
    }


}
