package com.libraryweb.demo.model;

import com.libraryweb.demo.services.AuthorDto;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table
public class AuthorsEntity {

    private int authorId;
    private String authorName;
    private String firstThreeBooks;
    private int rank;
    private List<BooksEntity> books = new ArrayList<BooksEntity>();

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(updatable = false, nullable = false)
    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    @Column(nullable = false)
    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    @Column(nullable = false)
    public String getFirstThreeBooks() {
        return firstThreeBooks;
    }

    public void setFirstThreeBooks(String firstThreeBooks) {
        this.firstThreeBooks = firstThreeBooks;
    }

    @Column(nullable = false)
    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    @ManyToMany(mappedBy = "authors")
    public List<BooksEntity> getBooks() {
        return books;
    }

    public void setBooks(List<BooksEntity> books) {
        this.books = books;
    }

    public AuthorDto fromEntityToDto(AuthorDto author) {
        author.setAuthorId(this.getAuthorId());
        author.setAuthorName(this.getAuthorName());
        author.setFirstThreeBooks(this.getFirstThreeBooks());
        author.setRank(this.getRank());
        return author;
    }
}
