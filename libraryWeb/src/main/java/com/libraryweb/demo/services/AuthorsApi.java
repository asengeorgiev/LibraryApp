package com.libraryweb.demo.services;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

public interface AuthorsApi {


    @GetMapping(path = "/allAuthors")
    ResponseEntity<AuthorDto[]> allAuthors();

    @PostMapping(path = "/createAuthor")
    void createBook(@RequestBody AuthorDto booksDto);

    @PutMapping(path = "/editAuthor")
    void editBook(@RequestBody AuthorDto authorDto);

    @RequestMapping(method = RequestMethod.DELETE, value = {"deleteAuthor/{id}"})
    void delete(@PathVariable Integer id);
}
