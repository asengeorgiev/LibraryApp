package com.libraryweb.demo.view;

import com.vaadin.data.HasValue;

import java.util.List;

public interface AuthorsView {


    void setupLayout();

    void navigate(String where);

    void setNavigationButton();

    Boolean caseInsensitiveContains(String where, String what);

    void onNameFilterTextChange(HasValue.ValueChangeEvent<String> event);

    void setTimeout(Runnable runnable, int delay);

    void insertFirstThreeBooks(List<DummyAuthor> allAuthors, List<DummyBook> allBooks);

    void addAuthorsGrid();

    void setBackGround();
}
