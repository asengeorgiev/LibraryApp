package com.libraryweb.demo.view;

import java.util.ArrayList;
import java.util.List;

public class DummyBook {
    private int bookId;
    private String name;
    private String author;
    private int numberOfPages;
    private String summary;
    private List<DummyAuthor> authors = new ArrayList<DummyAuthor>();

    public DummyBook() {
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(int numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public List<DummyAuthor> getAuthors() {
        return authors;
    }

    public void setAuthors(List<DummyAuthor> authors) {
        this.authors = authors;
    }
}
