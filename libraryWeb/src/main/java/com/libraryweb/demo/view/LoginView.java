package com.libraryweb.demo.view;

import com.vaadin.ui.Button;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;


public interface LoginView {

    TextField getTxtUsername();

    PasswordField getTxtPassword();

    Button getBtnLogin();

    void setupLayout();

    void addHeader();

    void addUsernameForm();

    void addPasswordForm();

    void navigate();


    void setEnterButton();

    void setBackGround();
}
