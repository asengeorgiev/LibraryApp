package com.libraryweb.demo.view;

import com.libraryweb.demo.presenter.LoginPresenter;
import com.vaadin.annotations.StyleSheet;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;

@SpringUI(path = "/")
@StyleSheet("styles.css")

public class LoginViewImpl extends UI implements LoginView {
    @Autowired
    private LoginPresenter loginPresenter;

    private VerticalLayout root;
    private TextField txtUsername;
    private PasswordField txtPassword;
    private Button btnLogin;


    @Override
    public TextField getTxtUsername() {
        return this.txtUsername;
    }

    @Override
    public PasswordField getTxtPassword() {
        return this.txtPassword;
    }

    @Override
    public Button getBtnLogin() {
        return this.btnLogin;
    }


    @Override
    protected void init(VaadinRequest vaadinRequest) {
        setupLayout();
        addHeader();
        addUsernameForm();
        addPasswordForm();
        setBackGround();
        setEnterButton();

    }

    @Override
    public void setupLayout() {
        root = new VerticalLayout();
        setContent(root);
        root.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);

    }

    @Override
    public void addHeader() {

        Label header = new Label(("Login"));
        header.addStyleName(ValoTheme.LABEL_H1);
        root.addComponent(header);


    }

    @Override
    public void addUsernameForm() {
        HorizontalLayout usernameLayout = new HorizontalLayout();
        txtUsername = new TextField("Username");
        txtUsername.addStyleName(ValoTheme.LABEL_H1);
        usernameLayout.addComponents(txtUsername);

        root.addComponent(usernameLayout);

    }

    @Override
    public void addPasswordForm() {
        HorizontalLayout passwordLayout = new HorizontalLayout();
        txtPassword = new PasswordField("Password");
        passwordLayout.addComponents(txtPassword);
        root.addComponent(passwordLayout);
        passwordLayout.addStyleName("darkBorder");
    }

    @Override
    public void navigate() {
        UI.getCurrent().getPage().setLocation("authors");
    }

    @Override
    public void setEnterButton() {
        HorizontalLayout enterButtonLayout = new HorizontalLayout();
        btnLogin = new Button("Enter");
        enterButtonLayout.addComponents(btnLogin);
        btnLogin.addStyleName(ValoTheme.BUTTON_PRIMARY);
        btnLogin.setIcon(VaadinIcons.BOOK);
        loginPresenter.setLoginView(this);

        btnLogin.addClickListener(click -> {
            if (loginPresenter.authenticateAndLogin()) {
                navigate();
            } else {
                Notification.show("Wrong ID or Password.");
            }
        });

        root.addComponent(enterButtonLayout);

    }

    @Override
    public void setBackGround() {
        Page.getCurrent().getStyles().add(
                ".v-ui { background: url(libraryPic.jpg) no-repeat center center fixed;" +
                        "-webkit-background-size: cover;" +
                        "-moz-background-size: cover;" +
                        "-o-background-size: cover;" +
                        "background-size: cover; }"
        );

    }


}
