package com.libraryweb.demo.presenter;

import com.libraryweb.demo.view.AuthorsView;
import com.libraryweb.demo.view.DummyAuthor;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;


@Component
public class AuthorsPresenterImpl implements AuthorsPresenter {

    private AuthorsView authorsView;

    @Override
    public void setAuthorsView(AuthorsView authorsView) {
        this.authorsView = authorsView;
    }

    @Override
    public DummyAuthor[] transferAllAuthors() {
        final String uri = "http://localhost:8010/allAuthors";
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<DummyAuthor[]> result = restTemplate.getForEntity(uri, DummyAuthor[].class);
        DummyAuthor[] allAuthors = result.getBody();

        return allAuthors;

    }


    @Override
    public void sendNewDataBackToDataBase(DummyAuthor dummyAuthor) {
        final String uri = "http://localhost:8010/createAuthor";
        RestTemplate restTemplate = new RestTemplate();
        DummyAuthor post = restTemplate.postForObject(uri, dummyAuthor, DummyAuthor.class);

    }

    @Override
    public void sendAuthorForEdit(DummyAuthor dummyAuthor) {
        final String uri = "http://localhost:8010/editAuthor";
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.put(uri, dummyAuthor);

    }

    @Override
    public void sendAuthorForDelete(Integer id) {
        final String url = "http://localhost:8010/deleteAuthor";
        Map<String, String> body = new HashMap<>();
        String idToString = String.valueOf(id);
        body.put("id", idToString);
        RestTemplate restTemplate = new RestTemplate();

        restTemplate.delete(url + "/" + id, body);


    }


}
