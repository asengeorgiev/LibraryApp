package com.libraryweb.demo.services;

import java.util.ArrayList;
import java.util.List;

public class AuthorDto {
    private int authorId;
    private String authorName;
    private String firstThreeBooks;
    private int rank;
    private List<BooksDto> books = new ArrayList<BooksDto>();

    public AuthorDto() {
    }


    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getFirstThreeBooks() {
        return firstThreeBooks;
    }

    public void setFirstThreeBooks(String firstThreeBooks) {
        this.firstThreeBooks = firstThreeBooks;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }
}
