package com.libraryweb.demo.services;

import com.libraryweb.demo.model.AuthorsEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorsRepository extends JpaRepository<AuthorsEntity,Integer> {



}
