package com.libraryweb.demo.services;

import com.libraryweb.demo.model.LoginEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<LoginEntity, Integer> {

    @Override
    List<LoginEntity> findAll();
}
