package com.libraryweb.demo.view;

import com.libraryweb.demo.presenter.AuthorsPresenter;
import com.libraryweb.demo.presenter.BooksPresenter;
import com.vaadin.data.Binder;
import com.vaadin.data.HasValue;
import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;

@Component
@SpringUI(path = "/authors")
public class AuthorsViewImpl extends UI implements AuthorsView {


    private AbsoluteLayout root;
    @Autowired
    private AuthorsPresenter authorsPresenter;
    @Autowired
    private BooksPresenter booksPresenter;
    private Button addButton;
    private ListDataProvider<DummyAuthor> authorsDataProvider;
    private List<DummyAuthor> modifiableAuthors2;
    Grid<DummyAuthor> authorsGrid;


    @PostConstruct
    public void postConstruct() {
        authorsPresenter.setAuthorsView(this);
    }

    @Override
    protected void init(VaadinRequest request) {
        setupLayout();
        addAuthorsGrid();
        setBackGround();
        setNavigationButton();
    }

    @Override
    public void setupLayout() {
        root = new AbsoluteLayout();
        root.setWidth("1680px");
        root.setHeight("1050px");
        setContent(root);

    }

    @Override
    public void navigate(String where) {
        UI.getCurrent().getPage().setLocation(where);
    }

    @Override
    public void setNavigationButton() {
        Button navButton = new Button("Go to books");
        navButton.addClickListener(click -> navigate("books"));
        root.addComponent(navButton, "left: 500 px; top: 10px;");
        Button logoutButton = new Button("Logout");
        logoutButton.addClickListener(click -> navigate("/"));
        root.addComponent(logoutButton, "left: 1345 px; top: 10px;");

    }

    @Override
    public Boolean caseInsensitiveContains(String where, String what) {
        return where.toLowerCase().contains(what.toLowerCase());
    }

    @Override
    public void onNameFilterTextChange(HasValue.ValueChangeEvent<String> event) {
        ListDataProvider<DummyAuthor> dataProvider = (ListDataProvider<DummyAuthor>) authorsGrid.getDataProvider();
        dataProvider.setFilter(DummyAuthor::getAuthorName, s -> caseInsensitiveContains(s, event.getValue()));
    }

    @Override
    public void setTimeout(Runnable runnable, int delay) {
        new Thread(() -> {
            try {
                Thread.sleep(delay);
                runnable.run();
            } catch (Exception e) {
                System.err.println(e);
            }
        }).start();
    }

    @Override
    public void insertFirstThreeBooks(List<DummyAuthor> allAuthors, List<DummyBook> allBooks) {
        int counter = 1;
        for (DummyAuthor anAuthor : allAuthors) {
            counter = 1;
            for (DummyBook aBook : allBooks) {
                if (aBook.getAuthor().contains(anAuthor.getAuthorName())) {
                    if (counter <= 2) {
                        anAuthor.setFirstThreeBooks(anAuthor.getFirstThreeBooks() + aBook.getName() + ", ");
                        counter++;
                    } else if (counter == 3) {
                        anAuthor.setFirstThreeBooks(anAuthor.getFirstThreeBooks() + aBook.getName());
                        break;
                    }

                }
            }
        }
    }

    @Override
    public void addAuthorsGrid() {
        HorizontalLayout gridLayout = new HorizontalLayout();
        authorsGrid = new Grid<DummyAuthor>(DummyAuthor.class);
        gridLayout.addComponents(authorsGrid);
        root.addComponent(gridLayout, "left: 150px; top: 50px;");
        authorsGrid.setWidth("1280px");
        authorsGrid.setHeight("580px");
        List<DummyAuthor> authorsFromDb = Arrays.asList(authorsPresenter.transferAllAuthors());
        List<DummyAuthor> modifiableAuthors = new ArrayList<>(authorsFromDb);
        List<DummyBook> allBooks = Arrays.asList(booksPresenter.transferAllBooks());
        authorsGrid.setItems(modifiableAuthors
                .stream()
                .sorted((e1, e2) -> Integer.compare(e1.getAuthorId(), e2.getAuthorId())));
        authorsGrid.setColumns("authorName", "firstThreeBooks", "rank", "authorId");
        authorsDataProvider = DataProvider.ofCollection(modifiableAuthors
                .stream()
                .sorted((e1, e2) -> Integer.compare(e1.getAuthorId(), e2.getAuthorId()))
                .collect(Collectors.toList()));
        authorsGrid.setDataProvider(authorsDataProvider);
        authorsGrid.getEditor().setEnabled(true);
        authorsGrid.getColumn("authorName").setEditorComponent(new TextField());
        // authorsGrid.getColumn("rank").setEditorComponent(new TextField());
        StringToIntegerConverter stringToIntConverter = new StringToIntegerConverter("Enter a number!");
        Binder<DummyAuthor> binder = authorsGrid.getEditor().getBinder();
        authorsGrid.getColumn("rank").setEditorBinding(binder
                .forField(new TextField())
                .withConverter(new StringToIntegerConverter("Enter a number!"))
                .bind(DummyAuthor::getRank, DummyAuthor::setRank));
        addButton = new Button("Add");
        root.addComponent(addButton, "left: 151px; top: 10px;");
        //FILTER
        TextField nameFilter = new TextField();
        nameFilter.setPlaceholder("Filter by name...");
        nameFilter.addValueChangeListener(this::onNameFilterTextChange);
        root.addComponent(nameFilter, "left: 295px; top: 10px;");
        authorsGrid.removeColumn("authorId");
        insertFirstThreeBooks(modifiableAuthors, allBooks);

        //CREATE
        addButton.addClickListener(e -> {
            DummyAuthor newAuthor = new DummyAuthor();
            newAuthor.setAuthorName("");
            newAuthor.setRank(0);
            newAuthor.setFirstThreeBooks("");
            modifiableAuthors.add(newAuthor);
            authorsPresenter.sendNewDataBackToDataBase(newAuthor);
            List<DummyAuthor> authorsFromDbForAdd = Arrays.asList(authorsPresenter.transferAllAuthors());
            insertFirstThreeBooks(authorsFromDbForAdd, allBooks);
            modifiableAuthors2 = new ArrayList<>(authorsFromDbForAdd);
            modifiableAuthors2.forEach(author -> {
                if (author.getAuthorName().equals("")) {
                    author.setFirstThreeBooks("");
                }
            });
            authorsGrid.setItems(modifiableAuthors2
                    .stream()
                    .sorted((e1, e2) -> Integer.compare(e1.getAuthorId(), e2.getAuthorId())));
            authorsDataProvider.refreshAll();
        });

        //UPDATE
        authorsGrid.getEditor().addSaveListener(e -> {
            authorsGrid.getDataProvider().refreshAll();
            authorsPresenter.sendAuthorForEdit(e.getBean());
        });

        //DELETE
        authorsGrid.addComponentColumn(author -> new Button("Delete", event -> {
            authorsGrid.getEditor().setEnabled(false);
            setTimeout(() -> authorsGrid.getEditor().setEnabled(true), 500);
            authorsPresenter.sendAuthorForDelete(author.getAuthorId());
            DummyAuthor removeFromModified = null;
            for (DummyAuthor dummyAuthor : modifiableAuthors) {
                if (dummyAuthor.getAuthorId() == author.getAuthorId()) {
                    removeFromModified = dummyAuthor;
                }
            }
            modifiableAuthors.remove(removeFromModified);
            List<DummyAuthor> authorsFromDbForAdd = Arrays.asList(authorsPresenter.transferAllAuthors());
            modifiableAuthors2 = new ArrayList<>(authorsFromDbForAdd);
            authorsGrid.setItems(modifiableAuthors2.stream().sorted(Comparator.comparingInt(DummyAuthor::getAuthorId))
                    .distinct().collect(Collectors.toList()));

            insertFirstThreeBooks(modifiableAuthors2, allBooks);
        }));
    }

    @Override
    public void setBackGround() {
        Page.getCurrent().getStyles().add(
                ".v-ui { background: url(gridsBackGround.jpeg) no-repeat center center fixed;" +
                        "-webkit-background-size: cover;" +
                        "-moz-background-size: cover;" +
                        "-o-background-size: cover;" +
                        "background-size: cover; }"
        );

    }


}
