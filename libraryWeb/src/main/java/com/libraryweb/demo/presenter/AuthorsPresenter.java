package com.libraryweb.demo.presenter;

import com.libraryweb.demo.view.AuthorsView;
import com.libraryweb.demo.view.DummyAuthor;

public interface AuthorsPresenter {

    void setAuthorsView(AuthorsView authorsView);

    DummyAuthor[] transferAllAuthors();

    void sendNewDataBackToDataBase(DummyAuthor dummyAuthor);

    void sendAuthorForEdit(DummyAuthor dummyAuthor);

    void sendAuthorForDelete(Integer id);
}
