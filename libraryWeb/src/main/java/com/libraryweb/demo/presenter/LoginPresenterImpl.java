package com.libraryweb.demo.presenter;

import com.libraryweb.demo.services.UserLoginDataDto;
import com.libraryweb.demo.view.LoginView;
import org.springframework.stereotype.Component;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

@Component
public class LoginPresenterImpl implements LoginPresenter {

    private LoginView loginView;


    @Override
    public boolean authenticateAndLogin() {
        String username = loginView.getTxtUsername().getValue();
        String password = loginView.getTxtPassword().getValue();

        Client client = ClientBuilder.newClient();
        UserLoginDataDto user = new UserLoginDataDto(username, password);
        Response res = client.target("http://localhost:8010/login").request()
                .post(Entity.json(user));

        String unwrapResponse = res.readEntity(String.class);


        boolean result;

        if (unwrapResponse.equalsIgnoreCase("true")) {
            result = true;
        } else {
            result = false;
        }

        return result;

    }

    @Override
    public void setLoginView(LoginView loginView) {
        this.loginView = loginView;
    }


}
