package com.libraryweb.demo.services;

import com.libraryweb.demo.model.LoginEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.core.MediaType;
import java.util.List;

@RestController
public class UsernameApiImpl implements UsernameApi {
    private UserRepository viewRepository;

    @Autowired
    public UsernameApiImpl(UserRepository viewRepository) {
        this.viewRepository = viewRepository;
    }

    @Override
    @PostMapping(path = "/login", consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    public String getUsernameAndPasswordFromRepo(@RequestBody UserLoginDataDto userdata) {
        String result = null;
        List<LoginEntity> users = viewRepository.findAll();

        String username = userdata.getUsername();
        String password = userdata.getPassword();

        for (LoginEntity user : users) {
            String uPass = user.getPassword();
            String uName = user.getUsername();
            if (uName.equals(username) && uPass.equals(password)) {
                result = "true";
                break;
            } else {
                result = "false";
            }
        }
        return result;
    }


}
