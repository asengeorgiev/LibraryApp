package com.libraryweb.demo.view;

import com.libraryweb.demo.presenter.BooksPresenter;
import com.vaadin.data.Binder;
import com.vaadin.data.HasValue;
import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;


@Component
@SpringUI(path = "/books")
public class BooksViewImpl extends UI implements BooksView {

    private AbsoluteLayout root;
    @Autowired
    private BooksPresenter booksPresenter;
    private Button addButton;
    private ListDataProvider<DummyBook> booksDataProvider;
    private List<DummyBook> modifiableBooks2;
    private Grid<DummyBook> booksGrid;


    @PostConstruct
    public void postConstruct() {
        booksPresenter.setBooksView(this);
    }

    @Override
    protected void init(VaadinRequest request) {
        setupLayout();
        addBooksGrid();
        setBackGround();
        setNavigationButton();
    }

    @Override
    public void setupLayout() {
        root = new AbsoluteLayout();
        root.setWidth("1680px");
        root.setHeight("1050px");
        setContent(root);

    }

    @Override
    public void navigate(String where) {
        UI.getCurrent().getPage().setLocation(where);
    }

    @Override
    public void setNavigationButton() {

        Button navButton = new Button("Go to authors");
        navButton.addClickListener(click -> navigate("authors"));
        root.addComponent(navButton, "left: 500 px; top: 10px;");
        Button logoutButton = new Button("Logout");
        logoutButton.addClickListener(click -> navigate("/"));
        root.addComponent(logoutButton, "left: 1345 px; top: 10px;");
    }

    @Override
    public Boolean caseInsensitiveContains(String where, String what) {
        return where.toLowerCase().contains(what.toLowerCase());
    }

    @Override
    public void onNameFilterTextChange(HasValue.ValueChangeEvent<String> event) {
        ListDataProvider<DummyBook> dataProvider = (ListDataProvider<DummyBook>) booksGrid.getDataProvider();
        dataProvider.setFilter(DummyBook::getName, s -> caseInsensitiveContains(s, event.getValue()));
    }

    @Override
    public void setTimeout(Runnable runnable, int delay) {
        new Thread(() -> {
            try {
                Thread.sleep(delay);
                runnable.run();
            } catch (Exception e) {
                System.err.println(e);
            }
        }).start();
    }

    @Override
    public void addBooksGrid() {
        HorizontalLayout gridLayout = new HorizontalLayout();
        booksGrid = new Grid<DummyBook>(DummyBook.class);
        gridLayout.addComponents(booksGrid);
        root.addComponent(gridLayout, "left: 150px; top: 50px;");
        booksGrid.setWidth("1280px");
        booksGrid.setHeight("580px");
        List<DummyBook> booksFromDb = Arrays.asList(booksPresenter.transferAllBooks());
        List<DummyBook> modifiableBooks = new ArrayList<>(booksFromDb);
        booksGrid.setItems(modifiableBooks.stream().sorted((e1, e2) -> Integer.compare(e1.getBookId(), e2.getBookId())));
        booksGrid.setColumns("name", "author", "numberOfPages", "summary", "bookId");
        booksDataProvider = DataProvider.ofCollection(modifiableBooks.stream().sorted((e1, e2) -> Integer.compare(e1.getBookId(), e2.getBookId())).collect(Collectors.toList()));
        booksGrid.setDataProvider(booksDataProvider);
        booksGrid.getEditor().setEnabled(true);
        booksGrid.getColumn("name").setEditorComponent(new TextField());
        booksGrid.getColumn("author").setEditorComponent(new TextField());
        StringToIntegerConverter stringToIntConverter = new StringToIntegerConverter("Enter a number!");
        Binder<DummyBook> binder = booksGrid.getEditor().getBinder();
        booksGrid.getColumn("numberOfPages").setEditorBinding(binder
                .forField(new TextField())
                .withConverter(new StringToIntegerConverter("Enter a number!"))
                .bind(DummyBook::getNumberOfPages, DummyBook::setNumberOfPages));
        TextField txt = new TextField();
        txt.setMaxLength(254);
        booksGrid.getColumn("summary").setEditorComponent(txt);
        addButton = new Button("Add");
        root.addComponent(addButton, "left: 151px; top: 10px;");
        TextField nameFilter = new TextField();
        nameFilter.setPlaceholder("Filter by name...");
        nameFilter.addValueChangeListener(this::onNameFilterTextChange);
        root.addComponent(nameFilter, "left: 295px; top: 10px;");
        booksGrid.removeColumn("bookId");

        //CREATE
        addButton.addClickListener(e -> {
            DummyBook newBook = new DummyBook();
            newBook.setName("-");
            newBook.setAuthor("-");
            newBook.setNumberOfPages(0);
            newBook.setSummary("-");
            modifiableBooks.add(newBook);
            booksPresenter.sendNewDataBackToDataBase(newBook);
            List<DummyBook> booksFromDbForAdd = Arrays.asList(booksPresenter.transferAllBooks());
            modifiableBooks2 = new ArrayList<>(booksFromDbForAdd);
            booksGrid.setItems(modifiableBooks2
                    .stream()
                    .sorted((e1, e2) -> Integer.compare(e1.getBookId(), e2.getBookId())));
            booksDataProvider.refreshAll();
        });

        //UPDATE
        booksGrid.getEditor().addSaveListener(e -> {
            booksGrid.getDataProvider().refreshAll();
            booksPresenter.sendBookForEdit(e.getBean());
        });

        //DELETE
        booksGrid.addComponentColumn(book -> new Button("Delete", event -> {
            booksGrid.getEditor().setEnabled(false);
            setTimeout(() -> booksGrid.getEditor().setEnabled(true), 500);
            booksPresenter.sendBookForDelete(book.getBookId());
            DummyBook removeFromModified = null;
            for (DummyBook dummyBook : modifiableBooks) {
                if (dummyBook.getBookId() == book.getBookId()) {
                    removeFromModified = dummyBook;
                }
            }
            modifiableBooks.remove(removeFromModified);
            List<DummyBook> booksFromDbForAdd = Arrays.asList(booksPresenter.transferAllBooks());
            modifiableBooks2 = new ArrayList<>(booksFromDbForAdd);
            booksGrid.setItems(modifiableBooks2
                    .stream()
                    .sorted(Comparator.comparingInt(DummyBook::getBookId))
                    .distinct().collect(Collectors.toList()));

        }));
    }

    @Override
    public void setBackGround() {
        Page.getCurrent().getStyles().add(
                ".v-ui { background: url(gridsBackGround.jpeg) no-repeat center center fixed;" +
                        "-webkit-background-size: cover;" +
                        "-moz-background-size: cover;" +
                        "-o-background-size: cover;" +
                        "background-size: cover; }"
        );

    }

}
