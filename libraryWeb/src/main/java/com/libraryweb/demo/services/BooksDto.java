package com.libraryweb.demo.services;

import java.util.ArrayList;
import java.util.List;

public class BooksDto {
    private int bookId;
    private String name;
    private String author;
    private int numberOfPages;
    private String summary;
    private List<AuthorDto> authors = new ArrayList<AuthorDto>();

    public BooksDto() {
    }

    public BooksDto(int bookId, String name, String author, int numberOfPages, String summary, List<AuthorDto> authors) {
        this.bookId = bookId;
        this.name = name;
        this.author = author;
        this.numberOfPages = numberOfPages;
        this.summary = summary;
        this.authors = authors;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(int numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public List<AuthorDto> getAuthors() {
        return authors;
    }

    public void setAuthors(List<AuthorDto> authors) {
        this.authors = authors;
    }
}
