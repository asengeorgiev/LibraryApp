package com.libraryweb.demo.view;


import com.vaadin.data.HasValue;

public interface BooksView {
    void setupLayout();

    void navigate(String where);

    void setNavigationButton();

    Boolean caseInsensitiveContains(String where, String what);

    void onNameFilterTextChange(HasValue.ValueChangeEvent<String> event);

    void setTimeout(Runnable runnable, int delay);

    public void addBooksGrid();

    void setBackGround();
}
