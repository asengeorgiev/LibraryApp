package com.libraryweb.demo.presenter;

import com.libraryweb.demo.view.BooksView;
import com.libraryweb.demo.view.DummyBook;

public interface BooksPresenter {

    void setBooksView(BooksView booksView);

    public DummyBook[] transferAllBooks();


    void sendNewDataBackToDataBase(DummyBook dummyBook);

    void sendBookForEdit(DummyBook dummyBook);

    void sendBookForDelete(Integer id);
}

