package com.libraryweb.demo.presenter;

import com.libraryweb.demo.view.BooksView;
import com.libraryweb.demo.view.DummyBook;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Component
public class BooksPresenterImpl implements BooksPresenter {

    private BooksView booksView;

    @Override
    public void setBooksView(BooksView booksView) {
        this.booksView = booksView;
    }

    @Override
    public DummyBook[] transferAllBooks() {
        final String uri = "http://localhost:8010/allBooks";
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<DummyBook[]> result = restTemplate.getForEntity(uri, DummyBook[].class);
        DummyBook[] allAuthors = result.getBody();

        return allAuthors;

    }



    @Override
    public void sendNewDataBackToDataBase(DummyBook dummyBook) {
        final String uri = "http://localhost:8010/createBook";
        RestTemplate restTemplate = new RestTemplate();
        DummyBook post = restTemplate.postForObject(uri, dummyBook, DummyBook.class);

    }

    @Override
    public void sendBookForEdit(DummyBook dummyBook){
        final String uri = "http://localhost:8010/editBook";
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.put(uri,dummyBook);

    }
    @Override
    public void sendBookForDelete(Integer id) {
        final String url = "http://localhost:8010/deleteBook";
        Map<String, String> body = new HashMap<>();
        String idToString = String.valueOf(id);
        body.put("id",idToString);
        RestTemplate restTemplate = new RestTemplate();

        restTemplate.delete(url + "/" + id, body);


    }

}
