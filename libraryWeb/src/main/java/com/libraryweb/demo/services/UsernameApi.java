package com.libraryweb.demo.services;

public interface UsernameApi {

    String getUsernameAndPasswordFromRepo(UserLoginDataDto userdata);

}
