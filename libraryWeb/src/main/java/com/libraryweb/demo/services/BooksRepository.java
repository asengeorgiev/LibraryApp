package com.libraryweb.demo.services;

import com.libraryweb.demo.model.BooksEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BooksRepository extends JpaRepository<BooksEntity,Integer> {
}
