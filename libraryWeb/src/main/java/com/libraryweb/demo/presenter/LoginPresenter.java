package com.libraryweb.demo.presenter;

import com.libraryweb.demo.view.LoginView;

public interface LoginPresenter {

    boolean authenticateAndLogin();
    void setLoginView(LoginView loginView);

}
